import ConfigParser
import datetime
import difflib
import fnmatch
import httplib
import json
import os
import re
import subprocess
import sys
import urllib
import psycopg2

# Directory Separator (DS)
DS = os.sep


class TccSyncHelpers:
	"""Helpers for QGIS Plugin"""

	def __init__(self):

		self.plugin_dir = os.path.dirname(__file__)
		self.config = ConfigParser.RawConfigParser()
		self.config_file_name = 'qgis_sync.cfg'
		self.config.read(self.plugin_dir + DS + self.config_file_name)

		if not self.configuration_file_check():
			# need to create a config file
			print 'arquivo de configuracao precisa ser criado'
			self.configuration_file_create()
			pass
		pass

	# Verifica se arquivo de configuracao ja foi criado
	def configuration_file_check(self):
		file = self.plugin_dir + DS + self.config_file_name
		check = os.path.isfile(file) and os.access(file, os.R_OK)
		return check

	# Cria um arquivo de configuracao com os dados iniciais
	def configuration_file_create(self):
		cfgfile = open(self.plugin_dir + DS + self.config_file_name,'wb')

		# add the settings to the structure of the file, and lets write it out...
		self.config.add_section('SERVER')
		self.config.set('SERVER', 'pg_host', 'localhost')
		self.config.set('SERVER', 'pg_port', '5432')
		self.config.set('SERVER', 'pg_database', 'tcc_sync')
		self.config.set('SERVER', 'pg_user', 'postgres')
		self.config.set('SERVER', 'pg_pass', '123456')

		with cfgfile as cfg:
			self.config.write(cfg)

		cfgfile.close()

		print self.config.sections()

	def configuration_load_option(self, section, option):
		try:
			return self.config.get(section, option)
		except ConfigParser.NoSectionError as e:
			return 'No section error'
		except ConfigParser.NoOptionError as e:
			return 'No option error'
		except:
			return 'Erro nao tratado'

	def configuration_save_options(self, dlg, section, options):
		"""Store configurations (options) in .cfg file"""
		cfgfile = open(self.plugin_dir + DS + self.config_file_name,'wb')

		# add section if not exists
		if not self.config.has_section(section):
			self.config.add_section(section)

		# add options
		for option in options:
			self.config.set(section, option, options[option])

		print self.config

		with cfgfile as cfg:
			self.config.write(cfg)

		cfgfile.close()

		return dlg.accept()

	# Testa a conexao com o servidor de banco de dados
	def connection_test(self, dlg):
		try:
			connectionString = "dbname='"+self.config.get('SERVER', 'pg_database')+"' user='"+self.config.get('SERVER', 'pg_user')+"' host='"+self.config.get('SERVER', 'pg_host')+"' password='"+self.config.get('SERVER', 'pg_pass')+"'"
			conn = psycopg2.connect(connectionString)
			dlg.lblTestResult.setText('Conexao realizada com sucesso!')
		except ConfigParser.NoSectionError as e:
			dlg.lblTestResult.setText("No section error")
		except:
			dlg.lblTestResult.setText('ERRO na conexao!')

	# Altera entre DialogA e DialogB
	def dialog_change(self, dlg_a, dlg_b):
		dlg_a.setVisible(False)
		dlg_a.setResult(1)

		dlg_b.setVisible(True)

		return dlg_a.result()

	def dialog_close(self, dlg):
		pass
