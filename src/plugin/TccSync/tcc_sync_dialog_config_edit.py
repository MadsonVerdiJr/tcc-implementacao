import os
import pdb

from PyQt4 import QtGui, uic

# Import Helpers
from tcc_sync_helpers import TccSyncHelpers

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'tcc_sync_dialog_config_edit.ui'))


class TccSyncDialogConfigEdit(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        """Constructor."""
        super(TccSyncDialogConfigEdit, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        self.configs = []
        self.btnCancel.clicked.connect(self.btn_cancel)
        self.btnSave.clicked.connect(self.btn_save)

        # initialize helpers
        self.helpers = TccSyncHelpers()

    def btn_save(self):
        # self.configuration_save_options()
        section = 'SERVER'
        options = {}
        options['pg_host'] = self.editHost.text()
        options['pg_port'] = self.editPort.text()
        options['pg_database'] = self.editPass.text()
        options['pg_user'] = self.editUser.text()
        options['pg_pass'] = self.editDatabase.text()
        print section
        print options
        self.editHost.clear()
        self.editPort.clear()
        self.editPass.clear()
        self.editUser.clear()
        self.editDatabase.clear()



        self.helpers.configuration_save_options(self, section, options)



    def btn_cancel(self):
        """Close function for btn"""
        self.editHost.clear()
        self.editPort.clear()
        self.editPass.clear()
        self.editUser.clear()
        self.editDatabase.clear()
        self.accept()
