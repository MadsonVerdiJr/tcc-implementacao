# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TccSync
                                 A QGIS plugin
 Sincronização com banco de dados
                             -------------------
        begin                : 2017-08-21
        copyright            : (C) 2017 by Madson Verdi Junior
        email                : madsonverdijr@gmail.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load TccSync class from file TccSync.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .tcc_sync import TccSync
    return TccSync(iface)
