import difflib
import os
import re
import subprocess
import sys
import httplib
import urllib
from contextlib import closing


DS = os.sep
current_path_file = os.path.realpath(__file__)

# pastas do projeto
project_folder = os.path.abspath(os.path.join(current_path_file,".." + DS + ".." + DS + ".."))
input_files_folder = project_folder + DS + 'arquivos_entrada' 
output_files_folder = project_folder + DS + 'arquivos_saida'
bin_folder = project_folder + DS + 'src' + DS + 'bin'

# nomes de arquivos de origem e atual - SHP
input_shp_file_name = input_files_folder + DS + 'mapa_rodoviario_rs_original' + DS + 'Rodovias_RioGrandeDoSul_2015_1.shp'
output_shp_file_name = input_files_folder + DS + 'mapa_rodoviario_rs' + DS + 'Rodovias_RioGrandeDoSul_2015_1.shp'

# nomes de arquivos de origem e atual - SQL
input_sql_file_name = output_files_folder + DS + 'Rodovias_RioGrandeDoSul_2015_1_original.sql'
output_sql_file_name = output_files_folder + DS + 'Rodovias_RioGrandeDoSul_2015_1.sql'
sync_sql_file_name = output_files_folder + DS + 'Rodovias_RioGrandeDoSul_2015_1_sync.sql'

# gerando arquivos para comparacao
args_input = bin_folder + DS + 'shp2pgsql.exe -W "latin1" -e ' + input_shp_file_name +  '  > ' + input_sql_file_name
args_output = bin_folder + DS + 'shp2pgsql.exe -W "latin1" -e ' + output_shp_file_name +  '  > ' + output_sql_file_name

try:
    retcode = subprocess.call(args_input, shell=True)
    if retcode < 0:
        print >>sys.stderr, "Child was terminated by signal", -retcode
    else:
        print >>sys.stderr, "Child returned", retcode
except OSError as e:
    print >>sys.stderr, "Execution failed:", e
    
try:
    retcode = subprocess.call(args_output, shell=True)
    if retcode < 0:
        print >>sys.stderr, "Child was terminated by signal", -retcode
    else:
        print >>sys.stderr, "Child returned", retcode
except OSError as e:
    print >>sys.stderr, "Execution failed:", e
    
    

# Diferenca entre os arquivos

original = open(input_sql_file_name, 'r')
updated = open(output_sql_file_name, 'r')

diff = difflib.ndiff(original.readlines(), updated.readlines())

delete_registers = ''.join(re.sub(r"[\s]*INSERT[\s]+INTO[\s]+\"(\w+)\" \([\"\w,]+\)\s+VALUES\s+\('(\d+)'[\,\'\d\w\W\s\/\-\(\)\.]+;[\s]*", r'DELETE FROM `\1` WHERE `id` = \2;\n', x[2:]) for x in diff if x.startswith('- '))
create_registers = ''.join(x[2:] for x in diff if x.startswith('+ '))

sql_export = open(sync_sql_file_name, 'wb');
sql_export.write(delete_registers);
sql_export.write(create_registers);
sql_export.close();

with closing(httplib.HTTPConnection("localhost", 8000)) as conn:
    headers = {"Content-type": "application/x-www-form-urlencoded"}
    params = urllib.urlencode({'value': delete_registers + create_registers})
    conn.request("POST", "/contents", params, headers)
    response = conn.getresponse()
    print response.getheaders()
    print response.read()


