import urllib
import httplib
import json
from datetime import datetime

conn = httplib.HTTPConnection("localhost", 8000)
headers = {"Accept": "application/json"}
params = urllib.urlencode({'date': datetime.now()})
print params
url = "/contents?" + params
print url

conn.request("GET", url, "",headers)

r1 = conn.getresponse()
print r1.status, r1.reason
data1 = json.loads(r1.read())
conn.close()

for x in data1:
	print x['value']

# import httplib
# import urllib 
# import json
# from urllib2 import urlopen
# from contextlib import closing

# def get_jsonparsed_data(url):
#     response = urlopen(url)
#     data = str(response.read())
#     return json.loads(data)

# get_jsonparsed_data('http://localhost:8000/contents')