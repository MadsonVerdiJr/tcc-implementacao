import httplib
import urllib
from contextlib import closing

with closing(httplib.HTTPConnection("localhost", 8000)) as conn:
    headers = {"Content-type": "application/x-www-form-urlencoded"}
    params = urllib.urlencode({'value': 'TESET'})
    conn.request("POST", "/contents", params, headers)
    response = conn.getresponse()
    print response.getheaders()
    print response.read()