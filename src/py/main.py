from contextlib import closing
from helpers import *
from datetime import datetime


shpFiles = search_files(project_folder + DS)

for file in shpFiles:
	print file
	print 'has synced?'
	print config_has_set(file, 'server')

	if config_has_set(file, 'server') == False:
		sql_original = create_sql(bin_folder, config_get(file, 'file_original'), output_files_folder, file, '_original')
		config_set(file, 'sql_original', sql_original)

		# data = server_retrieve_data(config_get('SERVER', 'host'), config_get('SERVER', 'port'), config_get(file, 'local'))
		
		create_shp(bin_folder, output_files_folder, file)

		sql_updated = create_sql(bin_folder, config_get(file, 'file_temp'), output_files_folder, file)
		config_set(file, 'sql_updated', sql_updated)
	else:
		print 'teste'
	sql_sync = create_diff_sql(sql_original, sql_updated, file)
	config_set(file, 'sql_updated', sql_sync)

