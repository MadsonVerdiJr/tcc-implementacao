import difflib
import os
import re
import subprocess

# Directory Separator
DS = os.sep

current_path_file = os.path.realpath(__file__)
project_folder = os.path.abspath(os.path.join(current_path_file,".." + DS + ".." + DS + ".."))

# original = open(project_folder + DS + 'arquivos_saida' + DS + 'shp2pgsql_t5_original.sql', 'r')
# updated = open(project_folder  + DS + 'arquivos_saida' + DS + 'shp2pgsql_t5.sql', 'r')

# original = open(project_folder + DS + 'arquivos_entrada' + DS + 'mapa_rodoviario_rs_original' + DS + 'Rodovias_RioGrandeDoSul_2015_1.shp', 'r')
# updated = open(project_folder  + DS + 'arquivos_entrada' + DS + 'mapa_rodoviario_rs' + DS + 'Rodovias_RioGrandeDoSul_2015_1.shp', 'r')

diff = difflib.ndiff(original.readlines(), updated.readlines())

delete_registers = ''.join(re.sub(r"[\s]*INSERT[\s]+INTO[\s]+\"(\w+)\" \([\"\w,]+\)\s+VALUES\s+\('(\d+)'[\,\'\d\w\s\/\-\(\)\.]+;[\s]*", r'DELETE FROM `\1` WHERE `id` = \2;', x[2:]) for x in diff if x.startswith('- '))
create_registers = ''.join(x[2:] for x in diff if x.startswith('+ '))

# Falta manipular a linha para pegar somente o GID do registro a ser removido
# delete_registers = re.subn(r"[\s]*INSERT[\s]+INTO[\s]+\"(\w+)\" \([\"\w,]+\)\s+VALUES\s+\('(\d+)'[\,\'\d\w\s\/\-\(\)\.]+;[\s]*", r'DELETE FROM `\1` WHERE `id` = \2;', delete_registers)

# INSERT INTO \"(\w+)\" \([\"\w,]+\) VALUES \('(\d+)'[\,'\d\w\s\/\-\(\)\.]+;

sql_export = open(project_folder + DS + 'arquivos_saida' +  DS + 'sync.sql', 'wb');
sql_export.write(delete_registers);
sql_export.close();