import ConfigParser
import datetime
import difflib
import fnmatch
import httplib
import json
import os
import re
import subprocess
import sys
import urllib


DS = os.sep

current_path_file = os.path.realpath(__file__)

# pastas do projeto
project_folder = os.path.abspath(os.path.join(current_path_file,".." + DS + ".." + DS + ".."))
input_files_folder = project_folder + DS + 'arquivos_entrada' 
output_files_folder = project_folder + DS + 'arquivos_saida'
bin_folder = project_folder + DS + 'src' + DS + 'bin'


# Buscar arquivos de uma determinada extencao e retorna uma lista com eles e sua data de atualizacao
def search_files(base_folder, extension = 'shp'):
	matches = []
	for root, dirnames, filenames in os.walk(input_files_folder + DS):
		for filename in fnmatch.filter(filenames, '*.' + extension):
			new_filename = filename.replace('.' + extension, '')
			date = modification_date(os.path.join(root, filename))
			local_aux = new_filename
			matches.append(new_filename)

			create_config(new_filename, [{'file_original': os.path.join(root, filename), 'local': date, 'server': ''}], base_folder)
	return matches



# Data de modificacao de um arquivo
def modification_date(filename):
	try:
		t = os.path.getmtime(filename)
	except os.error:
		pass
	return datetime.datetime.fromtimestamp(t)



# Criar arquivo de configuracao
def create_config(name, params = [{}], folder = project_folder + DS):
	config = ConfigParser.RawConfigParser()
	file = open(folder + 'qgis_sync.cfg', 'r')

	config.readfp(file)

	print not (config.has_section(name))

	if config.has_section(name) == False:
		config.add_section(name)
		for x in params:
			for param, value in x.items():
				config.set(name, param, value)
			pass
		pass
	else:
		for x in params:
			for param, value in x.items():
				if config.has_option(name, param) == False:
					config.set(name, param, value)
					pass
			pass
		pass
	
	with open(folder + 'qgis_sync.cfg', 'wb') as configfile:
			config.write(configfile)

	pass


# Carrega configuracoes
def config_items(section, folder = project_folder + DS):
	config = ConfigParser.RawConfigParser()
	file = open(folder + 'qgis_sync.cfg', 'r')

	config.readfp(file)
	return config.items(section)
	pass


# retorna se existe uma option definida
def config_get(section, option, folder = project_folder + DS):
	config = ConfigParser.RawConfigParser()
	file = open(folder + 'qgis_sync.cfg', 'r')

	config.readfp(file)
	return config.get(section, option)

# retorna se existe uma option definida
def config_set(section, option, value, folder = project_folder + DS):
	config = ConfigParser.RawConfigParser()
	file = open(folder + 'qgis_sync.cfg', 'r')

	config.readfp(file)
	config.set(section, option, value)

	with open(folder + 'qgis_sync.cfg', 'wb') as configfile:
			config.write(configfile)
	pass


# retorna se existe uma option definida
def config_has_set(section, option, folder = project_folder + DS):
	config = ConfigParser.RawConfigParser()
	file = open(folder + 'qgis_sync.cfg', 'r')

	config.readfp(file)
	return config.has_option(section, option) and config.get(section, option) != ''


def update_file_content(file, content):
	with open(file, 'a') as file:
			file.write(content)
	pass


def create_sql(bin_folder, input_file, output_files_folder, filename, sufix = ''):
	# nomes de arquivos de origem e atual - SQL
	input_sql_file_name = output_files_folder + DS + filename + sufix + '.sql'
	# print input_sql_file_name
	# gerando arquivos para comparacao
	args_input = bin_folder + DS + 'shp2pgsql.exe -W "latin1" -e ' + input_file +  '  > ' + input_sql_file_name
	
	# print args_input

	try:
		retcode = subprocess.call(args_input, shell=True)
		if retcode < 0:
			print >>sys.stderr, "Child was terminated by signal", -retcode
		else:
			print >>sys.stderr, "Child returned", retcode
	except OSError as e:
		print >>sys.stderr, "Execution failed:", e		

	return input_sql_file_name

def create_shp(bin_folder, output_files_folder, filename, sufix = ''):
	# nomes de arquivos de origem e atual - SQL
	shp_file_name = output_files_folder + DS + filename + sufix
	# print input_sql_file_name
	# gerando arquivos para comparacao
	args_input = bin_folder + DS + 'pgsql2shp.exe -f "' + shp_file_name + '" -h ' + config_get('SERVER', 'pg_host') + \
		' -P ' + config_get('SERVER', 'pg_pass') + \
		' -u ' + config_get('SERVER', 'pg_user') + \
		' ' + config_get('SERVER', 'pg_database') +\
		' ' + filename.lower()
	
	print args_input

	try:
		retcode = subprocess.call(args_input, shell=True)
		if retcode < 0:
			print >>sys.stderr, "Child was terminated by signal", -retcode
		else:
			print >>sys.stderr, "Child returned", retcode
	except OSError as e:
		print >>sys.stderr, "Execution failed:", e		

	config_set(filename, 'file_temp', shp_file_name + '.shp')

	return shp_file_name

def create_diff_sql(file_a, file_b, output_file):
	original = open(file_a, 'r')
	updated = open(file_b, 'r')

	output_file_full = output_files_folder + DS + output_file + '.sql'

	# print 'arquivo a: ' + file_a
	# print 'arquivo b: ' + file_b
	# print 'arquivo de saida: ' + output_file

	diff = difflib.ndiff(original.readlines(), updated.readlines())

	delete_registers = ''.join(re.sub(r"[\s]*INSERT[\s]+INTO[\s]+\"(\w+)\" \([\"\w,]+\)\s+VALUES\s+\('(\d+)'[\,\'\d\w\W\s\/\-\(\)\.]+;[\s]*", r'DELETE FROM `\1` WHERE `id` = \2;\n', x[2:]) for x in diff if x.startswith('- '))
	create_registers = ''.join(x[2:] for x in diff if x.startswith('+ '))
	# print output_file_full
	sql_export = open(output_file_full, 'wb');
	sql_export.write(delete_registers);
	sql_export.write(create_registers);
	sql_export.close();

	return output_file_full


# Recupera dados do servidor
def server_retrieve_data(hostname, port, date):
	conn = httplib.HTTPConnection(hostname, port)
	headers = {"Accept": "application/json"}
	params = urllib.urlencode({'date': date})
	# print params
	url = "/contents?" + params
	# print url

	conn.request("GET", url, "",headers)

	r1 = conn.getresponse()
	# print r1.status, r1.reason
	data1 = json.loads(r1.read())
	conn.close()

	# for x in data1:
	# 	print x['value']

	return data1

def server_send_data(host, port, content, date):
	with closing(httplib.HTTPConnection(host, port)) as conn:
	    headers = {"Content-type": "application/x-www-form-urlencoded"}
	    params = urllib.urlencode({'value': content}, {'created_at': date})
	    conn.request("POST", "/contents", params, headers)
	    response = conn.getresponse()
	pass