#!/bin/bash
#

# cat ../arquivos_saida/shp2pgsql_t5.sql | sort | diff ../arquivos_saida/shp2pgsql_t5_original.sql -
#
diff --ignore-all-space --ignore-blank-lines ../arquivos_saida/shp2pgsql_t5_original.sql ../arquivos_saida/shp2pgsql_t5.sql >> diff_t5_original.txt
diff --ignore-all-space --ignore-blank-lines ../arquivos_saida/shp2pgsql_t5.sql ../arquivos_saida/shp2pgsql_t5_original.sql >> diff_t5.txt