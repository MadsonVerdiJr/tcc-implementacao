#!/bin/bash

# Realiza a leitura de um arquivo Shape e converte para Postgres

# Teste 1:
shp2pgsql ../arquivos_entrada/mapa_rodoviario_rs/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t1.sql
shp2pgsql ../arquivos_entrada/mapa_rodoviario_rs_original/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t1_original.sql


# Teste 2:
# -W <encoding>
#               Specify the character encoding of Shapefile's attributes.  If this option is used the output will be encoded in UTF-8.
shp2pgsql -W "latin1" ../arquivos_entrada/mapa_rodoviario_rs/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t2.sql
shp2pgsql -W "latin1" ../arquivos_entrada/mapa_rodoviario_rs_original/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t2_original.sql


# Teste 3:
# -W <encoding>
#               Specify the character encoding of Shapefile's attributes.  If this option is used the output will be encoded in UTF-8.
#
# -D            Use  the  PostgreSQL  "dump" format for the output data. This can be combined with -a, -c and -d. It is much faster to load than the
#               default "insert" SQL format. Use this for very large data sets.
shp2pgsql -W "latin1" -D ../arquivos_entrada/mapa_rodoviario_rs/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t3.sql
shp2pgsql -W "latin1" -D ../arquivos_entrada/mapa_rodoviario_rs_original/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t3_original.sql


# Teste 4:
# -w     Output WKT format, instead of WKB.  Note that this can introduce coordinate drifts due to loss of precision.
#
# -W <encoding>
#               Specify the character encoding of Shapefile's attributes.  If this option is used the output will be encoded in UTF-8.
shp2pgsql -W "latin1" -w ../arquivos_entrada/mapa_rodoviario_rs/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t4.sql
shp2pgsql -W "latin1" -w ../arquivos_entrada/mapa_rodoviario_rs_original/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t4_original.sql


# Teste 5:
# -e     Execute each statement on its own, without using a transaction.  This allows loading of the majority of good  data  when  there  are
#        some  bad geometries that generate errors.  Note that this cannot be used with the -D flag as the "dump" format always uses a trans‐
#        action.
#
# -W <encoding>
#               Specify the character encoding of Shapefile's attributes.  If this option is used the output will be encoded in UTF-8.
shp2pgsql -W "latin1" -e ../arquivos_entrada/mapa_rodoviario_rs/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t5.sql
shp2pgsql -W "latin1" -e ../arquivos_entrada/mapa_rodoviario_rs_original/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t5_original.sql


# Teste 6:
# -e            Execute each statement on its own, without using a transaction.  This allows loading of the majority of good  data  when  there  are
#               some  bad geometries that generate errors.  Note that this cannot be used with the -D flag as the "dump" format always uses a trans‐
#               action.
#
# -D            Use  the  PostgreSQL  "dump" format for the output data. This can be combined with -a, -c and -d. It is much faster to load than the
#               default "insert" SQL format. Use this for very large data sets.
#
# -W <encoding>
#               Specify the character encoding of Shapefile's attributes.  If this option is used the output will be encoded in UTF-8.
shp2pgsql -W "latin1" -e -D ../arquivos_entrada/mapa_rodoviario_rs/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t6.sql
shp2pgsql -W "latin1" -e -D ../arquivos_entrada/mapa_rodoviario_rs_original/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t6_original.sql


# Teste 7:
# -e            Execute each statement on its own, without using a transaction.  This allows loading of the majority of good  data  when  there  are
#               some  bad geometries that generate errors.  Note that this cannot be used with the -D flag as the "dump" format always uses a trans‐
#               action.
#
# -D            Use  the  PostgreSQL  "dump" format for the output data. This can be combined with -a, -c and -d. It is much faster to load than the
#               default "insert" SQL format. Use this for very large data sets.
#
# -W <encoding>
#               Specify the character encoding of Shapefile's attributes.  If this option is used the output will be encoded in UTF-8.
shp2pgsql -W "latin1" -I -D -N insert ../arquivos_entrada/mapa_rodoviario_rs/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t7.sql
shp2pgsql -W "latin1" -I -D -N insert ../arquivos_entrada/mapa_rodoviario_rs_original/Rodovias_RioGrandeDoSul_2015_1.shp > ../arquivos_saida/shp2pgsql_t7_original.sql