shp2pgsql(1)                                                          PostGIS                                                         shp2pgsql(1)



NAME
       shp2pgsql - shapefile to postgis loader


SYNTAX
       shp2pgsql [options] shapefile [schema.]table


DESCRIPTION
       The shp2pgsql data loader converts ESRI Shape files into SQL suitable for insertion into a PostGIS/PostgreSQL database.

       Version: 1.1.5 (2006/10/06)


USAGE
       The  <shapefile>  is the name of the shape file, without any extension information. For example, 'roads' would be the name of the shapefile
       comprising the 'roads.shp', 'roads.shx', and 'roads.dbf' files.

       The <tablename> is the (optionally schema-qualified) name of the database table you want the data stored in in the  database.  Within  that
       table, the geometry will be placed in the 'geo_value' column by default.


OPTIONS
       The loader has several operating modes distinguished by command line flags:

       (Note that -a, -c, -d and -p are mutually exclusive.)

       -d     Drops the database table before creating a new table with the data in the Shape file.

       -a     Appends  data  from the Shape file into the database table. Note that to use this option to load multiple files, the files must have
              the same attributes and same data types.

       -c     Creates a new table and populates it from the Shape file. This is the default mode.

       -p     Only produces the table creation SQL code, without adding any actual data.  This can be used if you need to completely separate  the
              table creation and data loading steps.

       -D     Use  the  PostgreSQL  "dump" format for the output data. This can be combined with -a, -c and -d. It is much faster to load than the
              default "insert" SQL format. Use this for very large data sets.

       -w     Output WKT format, instead of WKB.  Note that this can introduce coordinate drifts due to loss of precision.

       -e     Execute each statement on its own, without using a transaction.  This allows loading of the majority of good  data  when  there  are
              some  bad geometries that generate errors.  Note that this cannot be used with the -D flag as the "dump" format always uses a trans‐
              action.

       -s [<FROM_SRID>:]<SRID>
              Creates and populates the geometry tables with the specified SRID.  If FROM_SRID is  given,  the  geometries  will  be  reprojected.
              Reprojection cannot be used with -D.

       -G     Use the geography type instead of geometry.  Geography is used to store lat/lon data.  At the moment the only spatial reference sup‐
              ported is 4326.

       -g <geometry_column>
              Specify the name of the geometry column (mostly useful in append mode).

       -k     Keep idendifiers case (column, schema and attributes). Note that attributes in Shapefile are usually all UPPERCASE.

       -i     Coerce all integers to standard 32-bit integers, do not create 64-bit bigints, even if the DBF header signature appears  to  warrant
              it.

       -S     Generate  simple  Geometries  instead  of  MULTIgeometries.  Shape  files  don't differ between LINESTRINGs and MULTILINESTRINGs, so
              shp2pgsql generates MULTILINESTRINGs by default. This switch will produce LINESTRINGs instead, but shp2pgsql will fail when it  hits
              a real MULTILINESTRING. The same works for POLYGONs vs. MULTIPOLYGONs.

       -W <encoding> Specify the character encoding of Shapefile's attributes.  If this option is used the output will be encoded in UTF-8.

       -I     Create a GiST index on the geometry column.

       -N <policy>
              Specify NULL geometries handling policy (insert,skip,abort).

       -T <tablespace>
              Specify  the tablespace for the new table.  Indexes will still use the default tablespace unless the -X parameter is also used.  The
              PostgreSQL documentation has a good description on when to use custom tablespaces.

       -X <tablespace>
              Specify the tablespace for the new table's indexes.  This applies to the primary key index, and the GIST spatial index if -I is also
              used.

       -?     Display version and usage information.


INSTALLATION
       To compile the program from source, simply run "make" in the source directory.  Then copy the binary in your shell search path (or wherever
       you like). This text is also available as a man page in the ../doc/man/ directory, ready for copying it into  the  manual  search  path  on
       unixoid systems.


EXAMPLES
       An example session using the loader to create an input file and uploading it might look like this:

       # shp2pgsql shaperoads roadstable roadsdb > roads.sql
       # psql -d roadsdb -f roads.sql

       A conversion and upload can be done all in one step using UNIX pipes:

       # shp2pgsql shaperoads roadstable roadsdb | psql -d roadsdb


AUTHORS
       Originally  written  by  Jeff  Lounsbury  <jeffloun@refractions.net>.   Improved  and maintained by Sandro Santilli <strk@refractions.net>.
       Includes small contributions and improvements by others.

       This application uses functionality from shapelib 1.2.9 by Frank Warmerdam <warmerda@gdal.velocet.ca> to read from ESRI Shape files.


SEE ALSO
       pgsql2shp(1)

       More information is available at http://postgis.refractions.net



                                                                                                                                      shp2pgsql(1)
