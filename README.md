Todo List:

1. [ ] Documentar resultados
1. [x] Transformar arquivos locais para um banco de dados
    1. [x] [Conversão nativa do POSTGIS](http://www.bostongis.com/pgsql2shp_shp2pgsql_quickguide.bqg)
    1. [x] Definir testes
    1. [x] Criar arquivos de testes
        1. [x] [GiST index](http://revenant.ca/www/postgis/workshop/indexing.html)
    1. [x] Gerar Queries a serem executadas no servidor
        1. [x] Insert
        1. [x] Remove
            1. [x] Corrigir expressão regular para converter função
1. [ ] Sincronizar dados entre local e remoto
    1. [ ] Enviar script com atualizações
        1. [ ] Definir se deve ser através de arquivo ou atualizar diretamente no BD?
    1. [ ] 
1. [ ] Plugin
    1. [ ] Capturar nome do arquivo (Projeto)
    1. [ ] aplicar transformações para o projeto ou para o arquivo?


Versao: 0.1.0